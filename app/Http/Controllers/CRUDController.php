<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class CRUDController extends Controller
{
    public function index()
    {
        $questions=Question::get();
        return view('questions',['questions'=>$questions]);
    }
    public function create()
    {
        return view('create');
    }
    public function newq(Request $request)
    {
        $questions = new Question();
        $questions->Question = $request->get('post');
        $questions->save();
        return redirect('/ques');
    }
}
